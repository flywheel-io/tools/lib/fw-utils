"""Parser tests."""

from datetime import datetime, timezone
from functools import partial

import pytest

import fw_utils

from .conftest import ALIASES, FIELDS


def test_parse_hrsize():
    assert fw_utils.parse_hrsize("1K") == 1024
    assert fw_utils.parse_hrsize("1KB") == 1024
    assert fw_utils.parse_hrsize("1 KB") == 1024
    assert fw_utils.parse_hrsize("1. KB") == 1024
    assert fw_utils.parse_hrsize("1.0 KB") == 1024
    assert fw_utils.parse_hrsize("1.0 kb") == 1024


def test_parse_hrsize_raises_on_invalid_str():
    with pytest.raises(ValueError):
        fw_utils.parse_hrsize("foo")


def test_parse_hrtime():
    assert fw_utils.parse_hrtime("0.1s") == 0.1
    assert fw_utils.parse_hrtime("1s") == 1
    assert fw_utils.parse_hrtime("1m") == 60
    assert fw_utils.parse_hrtime("1h") == 60 * 60
    assert fw_utils.parse_hrtime("1d") == 60 * 60 * 24
    assert fw_utils.parse_hrtime("1w") == 60 * 60 * 24 * 7
    assert fw_utils.parse_hrtime("51w 6d") == 60 * 60 * 24 * 363
    assert fw_utils.parse_hrtime("1y") == 60 * 60 * 24 * 365
    assert fw_utils.parse_hrtime("52w") == 60 * 60 * 24 * 364
    assert fw_utils.parse_hrtime("1y 1w") == 60 * 60 * 24 * 372


def test_parse_hrtime_raises_on_invalid_str():
    with pytest.raises(ValueError):
        fw_utils.parse_hrtime("foo")


def test_parse_url():
    url = "scheme+driver://user:pw@host:1234/path?q1&q2=a&q2=b,c#frag"  # gitleaks:allow
    parsed = dict(
        scheme="scheme",
        driver="driver",
        username="user",
        password="pw",
        host="host",
        port="1234",
        path="/path",
        q1="",
        q2=["a", "b", "c"],
        fragment="frag",
    )
    assert fw_utils.parse_url(url, split_params=True) == parsed


def test_parse_url_w_windows_drive():
    url = "fs://c:/path?q1&q2=a&q2=b,c#frag"
    parsed = dict(
        scheme="fs",
        host="c:",
        path="/path",
        q1="",
        q2=["a", "b", "c"],
        fragment="frag",
    )
    assert fw_utils.parse_url(url, split_params=True) == parsed


def test_parse_url_raises_on_invalid_url():
    with pytest.raises(ValueError):
        fw_utils.parse_url("foo")


parse_field_name = partial(fw_utils.parse_field_name, aliases=ALIASES, allowed=FIELDS)


@pytest.mark.parametrize(
    "field_name,expected",
    [
        ("routing", "external_routing_id"),  # infix
        ("group", "group._id"),  # direct alias
        ("grp", "group._id"),  # indirect alias
        ("grp.id", "group._id"),  # id alias
        ("g.l", "group.label"),  # prefix-based expansion
        ("prj", "project.label"),  # indirect alias
        ("sex", "subject.sex"),  # unique suffix
        ("weight", "session.weight"),  # unique suffix
        ("sess.time", "session.timestamp"),  # prefix-based expansion
        ("timestamp", "acquisition.timestamp"),  # direct alias
        ("acq.info.key", "acquisition.info.key"),  # info wildcard
        ("info.key", "file.info.key"),  # alias and info wildcard
        ("f.c", "file.classification"),
        ("classification.intent", "file.classification.intent"),
    ],
)
def test_parse_field_name(field_name, expected):
    assert parse_field_name(field_name) == expected


@pytest.mark.parametrize(
    "field_name,message",
    [
        ("foo", "invalid"),
        ("label", "ambiguous"),
        ("subj.l", "ambiguous"),
    ],
)
def test_parse_field_name_errors(field_name, message):
    with pytest.raises(ValueError, match=message):
        parse_field_name(field_name)


validate_pattern_field = partial(
    fw_utils.parse_field_name,
    allowed=["a.b", "a.c", "timestamp"],
)
Pattern = partial(fw_utils.Pattern, validate=validate_pattern_field)
TS = datetime(1999, 12, 31, tzinfo=timezone.utc)


@pytest.mark.parametrize(
    "string,pattern,expected",
    [
        ("x", "a.b", {"a": {"b": "x"}}),  # implicit group
        ("x", "{a.b}", {"a": {"b": "x"}}),  # simple match
        ("x", "{a.b:X}!i", {"a": {"b": "x"}}),  # case insensitive opt
        ("x", "{a.b:[^x]+}!r", {}),  # no match, group format (regex opt)
        ("x", "{a.b}/{a.c}", {}),  # no match, non-group part
        ("x", "{a.b}[/{a.c}]", {"a": {"b": "x"}}),  # opt syntax - no match
        ("x/y", "{a.b}/{a.c}", {"a": {"b": "x", "c": "y"}}),  # multiple groups
        ("x/y", "{a.b}[/{a.c}]", {"a": {"b": "x", "c": "y"}}),  # opt syntax - match
        ("x/y/z", "**/{a.b:z}", {"a": {"b": "z"}}),  # glob-like "**"
        ("x/y", "{a.b}/*", {"a": {"b": "x"}}),  # glob-like "*"
        ("x/y", "{a.b}.*", {}),  # glob-like "." - no match
        ("x.y", "{a.b}.*", {"a": {"b": "x"}}),  # glob-like "." - match
        ("xyy", "{a.b}y{2}", {"a": {"b": "x"}}),  # repeat pattern
        ("x/y/z", "{b}[/{c}]", {"a": {"b": "x", "c": "y/z"}}),  # non-eager
        ("1999-12-31", "{t}", {"timestamp": TS}),  # dt support
        ("31121999", "{t:%d%m%Y}", {"timestamp": TS}),  # dt with strptime
    ],
)
def test_pattern_match(string, pattern, expected):
    assert Pattern(pattern).match(string) == expected
    # re-instantiating a pattern from the canonic form should behave the same
    assert Pattern(str(Pattern(pattern))).match(string) == expected


@pytest.mark.parametrize(
    "pattern,error",
    [
        # parse_field_name() functionality
        ("{foo}", "invalid field: 'foo'"),
        ("{a}", "ambiguous field: 'a'"),
        # Pattern() functionality
        ("", "empty pattern"),
        ("{}", "empty capture group"),
        ("{a.b}!x", "invalid opts"),
        ("{{", "unexpected {"),
        ("}", "unexpected }"),
        ("{:}", "invalid capture group"),
        ("{", "unterminated {"),
        ("{t:%o}", "invalid strptime code"),
    ],
)
def test_pattern_raises_on_invalid_syntax(pattern, error):
    with pytest.raises(ValueError, match=error):
        Pattern(pattern)


def test_pattern_str():
    pattern = Pattern("b")
    assert str(pattern) == r"{a.b}"
    assert repr(pattern) == r"Pattern('{a.b}')"
    pattern = Pattern("{b:.*}!r")
    assert str(pattern) == r"{a.b:.*}!r"
    assert repr(pattern) == r"Pattern('{a.b:.*}!r')"


def test_pattern_validate_with_default_fmt():
    def validate(_):
        return "a", "[^/]+"

    # simplified re
    pattern = fw_utils.Pattern("{a}", validate=validate)
    assert pattern.canonic == "{a}"
    assert pattern.match("foo")
    assert not pattern.match("foo/bar")
    # raw re
    pattern = fw_utils.Pattern("{a}!r", validate=validate)
    assert pattern.canonic == "{a}!r"
    assert pattern.match("foo")
    assert not pattern.match("foo/bar")
    # validator returns None as default re -> match all
    pattern = fw_utils.Pattern("{a}", validate=lambda _: ("a", None))
    assert pattern.match("foo")
    assert pattern.match("foo/bar")
