"""File tests."""

import gzip
import io
import os
from pathlib import Path

import pytest

import fw_utils


@pytest.fixture
def path_obj(tmp_path):
    return tmp_path / "test.dat"


@pytest.fixture
def path_str(path_obj):
    return str(path_obj)


def test_binfile_raises_on_invalid_mode(path_str):
    with pytest.raises(ValueError):
        fw_utils.BinFile(path_str, mode="foo")


def test_binfile_path_str(path_str):
    with fw_utils.BinFile(path_str, mode="wb") as file:
        assert file.localpath == path_str
        assert file.metapath == path_str
        assert str(file) == f"BinFile('{path_str}', mode='wb')"
        file.write(b"path-str")
    with fw_utils.BinFile(path_str) as file:
        assert file.read() == b"path-str"
        assert str(file) == f"BinFile('{path_str}', mode='rb')"


def test_binfile_path_obj(path_obj, path_str):
    with fw_utils.BinFile(path_obj, mode="wb") as file:
        assert file.localpath == path_str
        assert file.metapath == path_str
        assert str(file) == f"BinFile('{path_str}', mode='wb')"
        file.write(b"path-obj")
    with fw_utils.BinFile(path_obj) as file:
        assert file.read() == b"path-obj"
        assert str(file) == f"BinFile('{path_str}', mode='rb')"


def test_binfile_bytes():
    with fw_utils.BinFile(b"bytes", mode="rb") as file:
        assert file.localpath is None
        assert file.metapath is None
        assert str(file) == f"BinFile('BytesIO/{hex(id(file.file))}', mode='rb')"
        assert file.read() == b"bytes"


def test_binfile_file_obj(path_obj):
    with path_obj.open(mode="wb") as wfile, fw_utils.BinFile(wfile, mode="wb") as file:
        assert file.localpath is None
        assert file.metapath is None
        assert str(file) == f"BinFile('BufferedWriter/{hex(id(file.file))}', mode='wb')"
        file.write(b"file-obj")
    with path_obj.open(mode="rb") as rfile, fw_utils.BinFile(rfile) as file:
        assert file.read() == b"file-obj"
        assert str(file) == f"BinFile('BufferedReader/{hex(id(file.file))}', mode='rb')"


def test_binfile_bytesio():
    bytesio = io.BytesIO()
    with fw_utils.BinFile(bytesio, mode="wb") as file:
        file.write(b"bytes-io")
    with fw_utils.BinFile(bytesio) as file:
        assert file.read() == b"bytes-io"
        assert str(file) == f"BinFile('BytesIO/{hex(id(bytesio))}', mode='rb')"
        assert file.localpath is None
        assert file.metapath is None


def test_binfile_metapath_override():
    with fw_utils.BinFile(io.BytesIO(), metapath="/override") as file:
        assert file.localpath is None
        assert file.metapath == "/override"


def test_binfile_raises_on_nonwritable_file_object(path_obj):
    path_obj.write_text("read-only")
    with path_obj.open(mode="rb") as file:
        with pytest.raises(ValueError):
            fw_utils.BinFile(file, mode="wb")
        with pytest.raises(ValueError):
            fw_utils.BinFile(fw_utils.BinFile(file), mode="wb")


def test_binfile_iterable():
    bytesio = io.BytesIO(b"foo\nbar\nbaz")
    with fw_utils.BinFile(bytesio) as file:
        assert next(file) == b"foo\n"
        assert list(file) == [b"bar\n", b"baz"]


def test_open_any_works_with_gzip(path_str):
    with fw_utils.open_any(gzip.open(path_str, mode="wb"), mode="wb") as file:
        file.write(b"foo")
    with fw_utils.open_any(gzip.open(path_str)) as file:
        assert file.read() == b"foo"


def test_open_any_raises_on_mode_mismatch(path_str):
    with fw_utils.open_any(path_str, mode="wb") as file, pytest.raises(ValueError):
        fw_utils.open_any(file, mode="rb")


def test_open_any_returns_existing_binfile_instance(path_str):
    with fw_utils.open_any(path_str, mode="wb") as file:
        assert fw_utils.open_any(file, mode="wb") is file


def test_binfile_works_with_streams():
    class Stream(io.BytesIO):
        def seekable(self) -> bool:
            return False

        def seek(self, pos, whence=0):
            raise io.UnsupportedOperation

    with fw_utils.BinFile(Stream(b"")) as file:
        assert not file.seekable()


def test_fileglob(tmp_path):
    files = [tmp_path / "a/file1.txt", tmp_path / "b/file2.txt"]
    for file in files:
        file.parent.mkdir()
        file.write_text("test")
    assert fw_utils.fileglob(str(tmp_path), recurse=True) == files


def test_tempfile_readable_writable_seekable():
    with fw_utils.TempFile() as tmp_file:
        assert tmp_file.readable()
        assert tmp_file.writable()
        assert tmp_file.seekable()


def test_temdir_chdir(tmp_path):
    os.chdir(tmp_path)
    assert os.getcwd() == str(tmp_path)
    with fw_utils.TempDir(chdir=True) as tempdir:
        assert isinstance(tempdir, Path)
        assert tempdir.exists()
        assert os.getcwd() == str(tempdir)
    assert not tempdir.exists()
    assert os.getcwd() == str(tmp_path)
