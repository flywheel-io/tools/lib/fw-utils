"""Cached state helper tests."""

import os
from unittest.mock import Mock, call

import fw_utils


def test_cached_descriptor(mocker):
    time = mocker.patch("fw_utils.state.time.time", return_value=0)
    mock = Mock()
    closable = Mock(spec=["close"])

    def get_one():
        mock.get_one()
        return 1

    def get_two(one):
        mock.get_two(one)
        yield one + 1
        mock.cleanup()

    def get_three(two):
        mock.get_three(two)
        return closable

    class Class:
        one = fw_utils.Cached(get_one)
        two = fw_utils.Cached(get_two, expire_in=1)
        three = fw_utils.Cached(get_three)

    # creating an instance with Cached attributes - shouldn't call the funcs
    instance = Class()
    assert isinstance(Class.one, fw_utils.Cached)
    assert mock.get_one.mock_calls == []
    assert mock.get_two.mock_calls == []
    assert mock.get_three.mock_calls == []

    # accessing the Cached attr values on the instance - should call the funcs
    assert instance.one == 1
    assert instance.two == 2
    assert instance.three
    # called once on access (reused when injected later)
    assert mock.get_one.mock_calls == [call()]
    # called once with 'one' dependency-injected
    assert mock.get_two.mock_calls == [call(1)]
    # called once with 'two' dependency-injected
    assert mock.get_three.mock_calls == [call(2)]

    # expired values should be expunged from the cache and re-initialized
    time.return_value += 1
    assert instance.two == 2
    # the cleanup got called (after yield in get_two())
    assert mock.cleanup.mock_calls == [call()]
    # and the func was called again to re-init
    assert mock.get_two.mock_calls == [call(1), call(1)]

    # deletion should trigger auto-closing
    del instance.three
    assert closable.close.mock_calls == [call()]

    # ham-fisted attribute setting works as expected
    instance.two = "two"
    assert instance.two == "two"


def test_tempenv():
    os.environ["foo"] = "foo"
    with fw_utils.TempEnv(clear=True, bar="bar") as env:
        env["baz"] = "baz"
        assert os.environ == env == {"bar": "bar", "baz": "baz"}
    assert os.environ["foo"] == "foo"
    assert "bar" not in os.environ
    assert "baz" not in os.environ
