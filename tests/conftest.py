"""Shared test config."""

import os

os.environ["TZ"] = "UTC"


FIELDS = [
    "external_routing_id",
    "group._id",
    "group.label",
    "project._id",
    "project.label",
    "subject._id",
    "subject.label",
    "subject.firstname",
    "subject.lastname",
    "subject.sex",
    "subject.tags",
    "subject.info.*",
    "session._id",
    "session.uid",
    "session.label",
    "session.age",
    "session.weight",
    "session.operator",
    "session.timestamp",
    "session.tags",
    "session.info.*",
    "acquisition._id",
    "acquisition.uid",
    "acquisition.label",
    "acquisition.timestamp",
    "acquisition.tags",
    "acquisition.info.*",
    "file.name",
    "file.type",
    "file.tags",
    "file.info.*",
    "file.classification",
    "file.classification.*",
]

ALIASES = {
    r"\.id$": "._id",
    r"^grp?(?=\.|$)": "group",
    r"^pro?j?(?=\.|$)": "project",
    r"^su(bj?)?(?=\\.|$)": "subject",
    r"^se(ss?)?(?=\.|$)": "session",
    r"^acq?(?=\.|$)": "acquisition",
    r"^group$": "group._id",
    r"^project$": "project.label",
    r"^subject$": "subject.label",
    r"^session$": "session.label",
    r"^acquisition$": "acquisition.label",
    r"^timestamp$": "acquisition.timestamp",
    r"^file$": "file.name",
    r"^info(?=\.|$)": "file.info",
    r"^classification(?=\.|$)": "file.classification",
}
