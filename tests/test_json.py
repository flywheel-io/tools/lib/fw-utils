"""JSON encoder tests."""

import json
from dataclasses import dataclass
from datetime import datetime

import pytest
from bson import ObjectId
from pydantic import BaseModel

from fw_utils import get_datetime


def test_json_encode():
    class Name:
        def __json__(self):
            return "Milo"

    @dataclass
    class DataclassPet:
        oid: ObjectId
        dob: datetime
        name: Name

    class PydanticPet(BaseModel, arbitrary_types_allowed=True):
        oid: ObjectId
        dob: datetime
        name: Name

    oid = ObjectId(oid_str := "d34db33fd34db33fd34db33f")
    dob = get_datetime(dob_str := "2000-01-01T00:00:00.000+00:00")
    assert (
        {"oid": oid_str, "dob": dob_str, "name": "Milo"}
        == roundtrip(DataclassPet(oid=oid, dob=dob, name=Name()))
        == roundtrip(PydanticPet(oid=oid, dob=dob, name=Name()))
        == roundtrip({"oid": oid, "dob": dob, "name": Name()})
    )

    with pytest.raises(TypeError):
        roundtrip(object())


def roundtrip(obj):
    return json.loads(json.dumps(obj))
