import re

from fastapi import FastAPI, Path
from fastapi.testclient import TestClient

from fw_utils.middleware import CacheControlMiddleware


def _test_client(cachable_paths, no_cache_paths):
    app = FastAPI()
    app.add_middleware(
        CacheControlMiddleware,
        cachable_paths=cachable_paths,
        no_cache_paths=no_cache_paths,
    )

    @app.get("/{root}/{sub}")
    def get(root: str = Path(...), sub: str = Path(...)):
        return {"root": root, "sub": sub}

    return TestClient(app)


def _check_response(response, headers=True):
    assert response.status_code == 200
    if headers:
        assert "Cache-Control" in response.headers
        assert (
            response.headers["Cache-Control"] == "no-store; no-cache; must-revalidate;"
        )
        assert "Pragma" in response.headers
        assert response.headers["Pragma"] == "no-cache"
    else:
        assert "Cache-Control" not in response.headers
        assert "Pragma" not in response.headers


def test_no_config():
    client = _test_client(None, None)

    r = client.get("/foo/bar")
    _check_response(r)

    r = client.get("/bar/baz")
    _check_response(r)


def test_cachable_paths():
    client = _test_client(["/foo", re.compile("^/bar/baz")], None)

    r = client.get("/foo/bar")
    _check_response(r, False)

    r = client.get("/bar/baz")
    _check_response(r, False)

    r = client.get("/bar/foobar")
    _check_response(r)


def test_no_cache_paths():
    client = _test_client(None, ["/foo", re.compile("^/bar/baz")])

    r = client.get("/foo/bar")
    _check_response(r)

    r = client.get("/bar/baz")
    _check_response(r)

    r = client.get("/bar/foobar")
    _check_response(r, False)


def test_cachable_and_no_cache():
    client = _test_client(["/bar/bazbar"], [re.compile("^/bar/[bf]")])

    r = client.get("/foo/bar")
    _check_response(r, False)

    r = client.get("/bar/baz")
    _check_response(r)

    r = client.get("/bar/foo")
    _check_response(r)

    r = client.get("/bar/bazbar")
    _check_response(r)
