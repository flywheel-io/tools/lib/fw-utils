"""Filter tests."""

from functools import partial

import pytest

import fw_utils

FILTER_FACTORY = {
    "auto": fw_utils.AutoFilter,
    "bool": fw_utils.BoolFilter,
    "dt": fw_utils.TimeFilter,
    "epoch": fw_utils.TimeFilter,
    "iso": fw_utils.TimeFilter,
    "label": fw_utils.StringFilter,
    "null": fw_utils.NullFilter,
    "num": fw_utils.NumberFilter,
    "set": fw_utils.SetFilter,
    "size": fw_utils.SizeFilter,
}
Filter = partial(fw_utils.IncludeExcludeFilter, factory=FILTER_FACTORY)


def test_filter_parse_raises_on_invalid_expression():
    with pytest.raises(ValueError):
        Filter(include=["foo"])


@pytest.mark.parametrize(
    "val, expr, match",
    [
        (None, "auto=null", True),
        (True, "auto=true", True),
        (True, "auto=false", False),
        (2.0, "auto>1", True),
        (2.0, "auto<=1", False),
        (None, "auto>foo", False),
        (3.0, "auto=~foo", False),
        ("foo", "auto=~fo+", True),
        ("foo", "auto!~fo+", False),
        ("foo", "auto!=bar", True),
        (["a", "b"], "auto=a", True),
        (["a", "b"], "auto!=a", False),
        (["a", "b"], "auto=~a", True),
        (["a", "b"], "auto!~a", False),
        (["a", "b"], "auto=c", False),
        (["a", "b"], "auto!=c", True),
    ],
)
def test_auto_filter(val, expr, match):
    obj = fw_utils.AttrDict(auto=val)

    include = Filter(include=[expr])
    assert include.match(obj) == match
    assert include.match(val) == match

    exclude = Filter(exclude=[expr])
    assert exclude.match(obj) == (not match)
    assert exclude.match(val) == (not match)


@pytest.mark.parametrize(
    "val, expr, match",
    [
        (None, "null=None", True),
        (None, "null!=NIL", False),
        ("foo", "null!=nil", True),
        ("foo", "null=null", False),
    ],
)
def test_null_filter(val, expr, match):
    obj = fw_utils.AttrDict(null=val)

    include = Filter(include=[expr])
    assert include.match(obj) == match
    assert include.match(val) == match

    exclude = Filter(exclude=[expr])
    assert exclude.match(obj) == (not match)
    assert exclude.match(val) == (not match)


def test_null_filter_raises_on_invalid_null():
    with pytest.raises(ValueError):
        Filter(include=["null=foo"])


@pytest.mark.parametrize(
    "val, expr, match",
    [
        (True, "bool=true", True),
        (True, "bool=1", True),
        (False, "bool=FALSE", True),
        (False, "bool=0", True),
        (None, "bool=1", False),
        (None, "bool=0", False),
    ],
)
def test_bool_filter(val, expr, match):
    obj = fw_utils.AttrDict(bool=val)

    include = Filter(include=[expr])
    assert include.match(obj) == match
    assert include.match(val) == match

    exclude = Filter(exclude=[expr])
    assert exclude.match(obj) == (not match)
    assert exclude.match(val) == (not match)


def test_bool_filter_raises_on_invalid_bool():
    with pytest.raises(ValueError):
        Filter(include=["bool=foo"])


@pytest.mark.parametrize(
    "val, expr, match",
    [
        ("Foo", "label=fOO", True),
        ("Foo", "label==fOO", True),  # == compat
        ("dir/file.txt", "label=dir/file.txt", True),
        ("dir/file.txt", "label!=dir/file.txt", False),
        ("dir/file.txt", "label=~dir/*.txt", True),
        ("dir/file.txt", "label!~dir/*.txt", False),
        ("dir/file.txt", "label=~dir/*.zip", False),
        ("dir/file.txt", "label!~dir/*.zip", True),
        ("file.txt", "label=~^*.txt", True),
        ("dir/file.txt", "label=~^*.txt", False),
        ("dir/file.txt", "label=~*/*.txt", True),
        ("dir1/dir2/file.txt", "label=~**/*.txt", True),
        ("dir1/dir2/file.txt", "label=~**/*.txt", True),
        ("1.1", r"label=~\uid", False),
        ("1.3.6.1.4.1.01.5", r"label=~^\uid$", False),
        ("1.3.6.1.4.1.14519.5", r"label=~\uid", True),
        ("MR.1.3.6.1.4.1.14519.5", r"label=~\uid", True),
        ("1.3.6.1.4.1.14519.5.dcm", r"label=~\uid.dcm", True),
        (r"\uid", r"label=\uid", True),
        (r"\uid", r"label=~\\uid", True),
        (None, "label=~dir", False),
        (None, "label!~dir", False),
    ],
)
def test_string_filter(val, expr, match):
    obj = fw_utils.AttrDict(label=val)

    include = Filter(include=[expr])
    assert include.match(obj) == match
    assert include.match(val) == match

    exclude = Filter(exclude=[expr])
    assert exclude.match(obj) == (not match)
    assert exclude.match(val) == (not match)


def test_string_filter_raises_on_invalid_regex():
    with pytest.raises(ValueError):
        Filter(include=["label=~foo("])


def test_string_filter_raises_on_invalid_op():
    with pytest.raises(ValueError):
        Filter(include=["label~=foo"])


def test_size_filter_raises_on_invalid_op():
    with pytest.raises(ValueError):
        Filter(include=["size=~1K"])


def test_size_filter_raises_on_invalid_size():
    with pytest.raises(ValueError):
        Filter(include=["size>foo"])


@pytest.mark.parametrize(
    "val, expr, match",
    [
        (1024, "size<=1K", True),
        (1024, "size<=1023", False),
        (1024, "size>=1K", True),
        (1024, "size>=1025", False),
        (1024, "size!=2K", True),
        (1024, "size!=1K", False),
        (1024, "size=1024", True),
        (1024, "size=1023", False),
        (1024, "size=1K", True),
        (1024, "size=2K", False),
        (1024, "size<2K", True),
        (1024, "size<1K", False),
        (1024, "size>1K", False),
        (1024, "size>512B", True),
        (None, "size>1K", False),
    ],
)
def test_size_filter(val, expr, match):
    obj = fw_utils.AttrDict(size=val)

    include = Filter(include=[expr])
    assert include.match(obj) == match
    assert include.match(val) == match

    exclude = Filter(exclude=[expr])
    assert exclude.match(obj) == (not match)
    assert exclude.match(val) == (not match)


def test_time_filter_raises_on_invalid_op():
    with pytest.raises(ValueError):
        Filter(include=["iso=~2020"])


def test_time_filter_raises_on_invalid_time():
    with pytest.raises(ValueError):
        Filter(include=["iso>foo"])


def test_time_filter_returns_false_with_none_value():
    obj = fw_utils.AttrDict(iso=None)
    include = Filter(include=["iso>2000"])
    assert not include.match(obj)
    assert not include.match(obj.iso)


@pytest.mark.parametrize(
    "time, expr, match",
    [
        ("1999-12-31 23:59:59", "<=1999", True),
        ("1999-12-31 23:59:59", "<=1998", False),
        ("1999-12-31 23:59:59", "<2000", True),
        ("1999-12-31 23:59:59", "<1999", False),
        ("1999-12-31 23:59:59", ">1998", True),
        ("1999-12-31 23:59:59", ">1999", False),
        ("1999-12-31 23:59:59", ">=199912", True),
        ("1999-12-31 23:59:59", ">=200001", False),
        ("1999-12-31 23:59:59", ">=1999-12", True),
        ("1999-12-31 23:59:59", ">=2000-01", False),
        ("1999-12-31 23:59:59", "!=1999-12-30", True),
        ("1999-12-31 23:59:59", "!=1999-12-31", False),
        ("1999-12-31 23:59:59", "!=1999-12-31 23:58", True),
        ("1999-12-31 23:59:59", "!=1999-12-31 23:59", False),
        ("1999-12-31 23:59:59", "=1999-12-31 23:59", True),
        ("1999-12-31 23:59:59", "=1999-12-31T23:59:59", True),
        ("1999-12-31 22:59:59", "=1999-12-31T23:59:59+0100", True),
        ("1999-12-31 23:59:59", "=1999-12-31T22:59:59-0100", True),
        ("1999-12-31 23:59:59", "=1999-12-31T23:59:59+0000", True),
        ("1999-12-31T23:59:59.999Z", "=1999-12-31T23:59:59.999Z", True),
    ],
)
def test_time_filter(time, expr, match):
    time = fw_utils.get_datetime(time)
    obj = fw_utils.AttrDict(dt=time, epoch=int(time.timestamp()), iso=time.isoformat())

    for filt in [f"dt{expr}", f"epoch{expr}", f"iso{expr}"]:
        include = Filter(include=[filt])
        assert include.match(obj) == match
        assert include.match(obj.dt) == match
        assert include.match(obj.epoch) == match
        assert include.match(obj.iso) == match

        exclude = Filter(exclude=[filt])
        assert exclude.match(obj) == (not match)
        assert exclude.match(obj.dt) == (not match)
        assert exclude.match(obj.epoch) == (not match)
        assert exclude.match(obj.iso) == (not match)


def test_num_filter_raises_on_invalid_op():
    with pytest.raises(ValueError):
        Filter(include=["num=~1"])


def test_num_filter_raises_on_invalid_size():
    with pytest.raises(ValueError):
        Filter(include=["num>foo"])


@pytest.mark.parametrize(
    "val, expr, match",
    [
        (10, "num<=10", True),
        (10, "num<=10.1", True),
        (10, "num>=10", True),
        (10, "num>=10.1", False),
        (10, "num!=20", True),
        (10, "num!=10", False),
        (10, "num=10", True),
        (10, "num=10.1", False),
        (None, "num>10.1", False),
    ],
)
def test_num_filter(val, expr, match):
    obj = fw_utils.AttrDict(num=val)

    include = Filter(include=[expr])
    assert include.match(obj) == match
    assert include.match(val) == match

    exclude = Filter(exclude=[expr])
    assert exclude.match(obj) == (not match)
    assert exclude.match(val) == (not match)


def test_set_filter_raises_on_invalid_op():
    with pytest.raises(ValueError):
        Filter(include=["set>1"])


def test_set_filter_raises_on_invalid_re():
    with pytest.raises(ValueError, match="nothing to repeat"):
        Filter(include=["set=~**!r"])


@pytest.mark.parametrize(
    "val, expr, match",
    [
        (["a b"], "set=a b", True),
        (["a b"], "set=A b", True),
        (["A b"], "set=a b", True),
        (["a b"], "set=b", False),
        (["a b"], "set!=b", True),
        (["a b"], "set=~b", True),
        (["a b"], "set=~B", True),
        (["A b"], "set=~a", True),
        (["a b"], "set=~^b", False),
        (["a b"], "set!~b", False),
        (["a b"], "set!~c", True),
        ([], "set=~b", False),
        ([], "set!~b", True),
        (None, "set=a", False),
        (None, "set!=a", True),
        (["A", "b"], "set!~a", False),
        ({}, "set=a", False),
    ],
)
def test_set_filter(val, expr, match):
    obj = fw_utils.AttrDict(set=val)

    include = Filter(include=[expr])
    assert include.match(obj) == match
    assert include.match(val) == match

    exclude = Filter(exclude=[expr])
    assert exclude.match(obj) == (not match)
    assert exclude.match(val) == (not match)


def test_filter_repr():
    filt = Filter(include=["label=~txt", "label=~bat"])
    assert repr(filt) == (
        "IncludeExcludeFilter(include=['label=~txt','label=~bat'], exclude=[])"
    )


def test_expression_filter_repr():
    class TestFilter(fw_utils.ExpressionFilter):
        def match(self, value):
            return True

    filt = TestFilter("test", "=", "foo")
    assert repr(filt) == "TestFilter('test', '=', 'foo')"


def test_filter_exclude_only():
    obj = fw_utils.AttrDict(label="foo", epoch=0)
    filt = Filter(include=["label=~foo"], exclude=["epoch<2000"])
    assert not filt.match(obj)
    assert filt.match(obj, exclude_only=["label"])
