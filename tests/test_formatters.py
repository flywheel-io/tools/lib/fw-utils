"""Formatter tests."""

from datetime import datetime
from functools import partial

import pytest

import fw_utils


def test_pluralize():
    # test regular noun
    assert fw_utils.pluralize("file") == "files"
    # test pre-registered irregular nouns
    assert fw_utils.pluralize("study") == "studies"
    assert fw_utils.pluralize("series") == "series"
    assert fw_utils.pluralize("analysis") == "analyses"
    # test custom irregular noun
    assert fw_utils.pluralize("body", "bodies") == "bodies"
    # test custom irregular noun got registered
    assert fw_utils.pluralize("body") == "bodies"


def test_quantify():
    assert fw_utils.quantify(0, "file") == "0 files"
    assert fw_utils.quantify(1, "file") == "1 file"
    assert fw_utils.quantify(2, "file") == "2 files"


def test_hrsize():
    assert fw_utils.hrsize(1) == "1B"
    assert fw_utils.hrsize(999) == "999B"
    assert fw_utils.hrsize(1000) == "1K"
    assert fw_utils.hrsize(1 << 10) == "1K"
    assert fw_utils.hrsize(9 << 10) == "9.2K"
    assert fw_utils.hrsize(10 << 10) == "10K"
    assert fw_utils.hrsize(999 << 10) == "1M"
    assert fw_utils.hrsize(1000 << 10) == "1M"
    assert fw_utils.hrsize(1 << 30) == "1.1G"
    assert fw_utils.hrsize(1 << 40) == "1.1T"
    assert fw_utils.hrsize(1 << 50) == "1.1P"
    assert fw_utils.hrsize(1 << 60) == "1.2E"
    assert fw_utils.hrsize(1 << 70) == "1.2Z"
    assert fw_utils.hrsize(1 << 80) == "1.2Y"
    assert fw_utils.hrsize(1 << 90) == "1238Y"


def test_hrtime():
    assert fw_utils.hrtime(0.1) == "0.1s"
    assert fw_utils.hrtime(1) == "1s"
    assert fw_utils.hrtime(10) == "10s"
    assert fw_utils.hrtime(60) == "1m"
    assert fw_utils.hrtime(90) == "1m 30s"
    assert fw_utils.hrtime(60 * 60) == "1h"
    assert fw_utils.hrtime(60 * 60 * 24) == "1d"
    assert fw_utils.hrtime(60 * 60 * 24 * 7) == "1w"
    assert fw_utils.hrtime(60 * 60 * 24 * 363) == "51w 6d"
    assert fw_utils.hrtime(60 * 60 * 24 * 364) == "52w"
    assert fw_utils.hrtime(60 * 60 * 24 * 365) == "1y"
    assert fw_utils.hrtime(60 * 60 * 24 * 366) == "1y"
    assert fw_utils.hrtime(60 * 60 * 24 * 372) == "1y 1w"


def test_format_url():
    url = "scheme+drv://user:pw@host:1234/path?q1&q2=a&q3=b&q3=c#frag"  # gitleaks:allow
    parsed = dict(
        scheme="scheme",
        driver="drv",
        username="user",
        password="pw",
        host="host",
        port="1234",
        path="/path",
        q1="",
        q2="a",
        q3=["b", "c"],
        fragment="frag",
    )
    assert fw_utils.format_url(**parsed) == url


def test_report_progress(mocker):
    messages = []
    perf_counter = mocker.patch("fw_utils.formatters.time.perf_counter")
    perf_counter.return_value = 0
    items = range(2)
    for _ in fw_utils.report_progress(items, callback=messages.append):
        perf_counter.return_value += 1
    assert messages == [
        "1/2 (50.0%) [1s, 1.0/s]",
        "2/2 (100.0%) [2s, 1.0/s]",
    ]

    messages.clear()
    perf_counter.return_value = 0
    items = (_ for _ in range(2))  # generator - no __len__
    for _ in fw_utils.report_progress(items, callback=messages.append):
        perf_counter.return_value += 1
    assert messages == [
        "1 [1s, 1.0/s]",
        "2 [2s, 1.0/s]",
    ]


def test_timer(mocker):
    start, elapsed = 0, 12.3
    mocker.patch("time.perf_counter", side_effect=[start, start + elapsed])
    timer = fw_utils.Timer(files=2, bytes=18 << 20)
    assert timer.report() == "2 files|19M in 12s [0.2/s|1.5M/s]"


validate_template_field = partial(
    fw_utils.parse_field_name,
    aliases={
        r"^file$": "file.name",
        r"^info(?=\.|$)": "file.info",
    },
    allowed=[
        "session.timestamp",
        "acquisition.timestamp",
        "file.name",
        "file.info.*",
        "path",
    ],
)
Template = partial(fw_utils.Template, validate=validate_template_field)
TEMPLATE_DATA = {
    "session.timestamp": datetime(1999, 12, 31, 23, 59, 59),
    "acquisition.timestamp": datetime(1999, 12, 31, 23, 59, 59),
    "file.name": "scan.log",
    "file.info.key": "value",
    "path": "foo/bar.txt",
}


@pytest.mark.parametrize(
    "template,expected",
    [
        # parse_field_name() functionality
        ("{file}", "scan.log"),  # alias
        ("{f.n}", "scan.log"),  # prefix expansion
        ("{name}", "scan.log"),  # unique suffix expansion
        ("{info.key}", "value"),  # alias and wildcard
        # Template() functionality
        ("file.name", "scan.log"),  # implicit format block
        ("file.name/.log/", "scan"),  # regex substitution
        ("file.name:X>7.4", "XXXscan"),  # string format (pad.trunc)
        ("file.info.missing|oops", "oops"),  # default
        ("acquisition.timestamp:%Y%m%d", "19991231"),  # dt format
        ("{acq.time:%Y%m%d}/{acq.time:%H%M%S}", "19991231/235959"),  # multi blocks/fmts
        ("\\{{file.name}\\}", r"\{scan.log\}"),  # escaped curly
        (r"{path/(.+)\/.*\.(.+)/\1.\2}", "foo.txt"),  # escaped slash in subst
    ],
)
def test_template_format(template, expected):
    # the template should format as expected
    assert Template(template).format(TEMPLATE_DATA) == expected
    # re-instantiating a template from the canonic form should behave the same
    assert Template(str(Template(template))).format(TEMPLATE_DATA) == expected


@pytest.mark.parametrize(
    "template,error",
    [
        # parse_field_name() functionality
        ("{foo.bar}", "invalid field: 'foo.bar'"),
        ("{timestamp}", "ambiguous field: 'timestamp'"),
        # Template() functionality
        ("", "empty template"),
        ("{}", "empty format block"),
        ("{{", "unexpected {"),
        ("}", "unexpected }"),
        ("{:}", "invalid format block"),
        ("{", "unterminated {"),
    ],
)
def test_template_raises_on_invalid_syntax(template, error):
    with pytest.raises(ValueError, match=error):
        Template(template)


def test_template_str():
    template = Template("file")
    assert str(template) == "{file.name}"
    assert repr(template) == "Template('{file.name}')"
