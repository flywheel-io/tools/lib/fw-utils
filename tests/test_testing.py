"""Test testing utils."""

import re
from unittest.mock import Mock, call

import pytest

import fw_utils


def test_assert_like():
    expected = dict(literal="val", list=[..., 2, 3, ...], regex=re.compile(r"\d"))
    got = dict(literal="val", list=[1, 2, 3, 4], regex="1", extra="extra")
    fw_utils.assert_like(expected, got)


def test_assert_like_disallow_extra():
    expected = dict(key="val")
    got = dict(key="val", extra="extra")
    with pytest.raises(AssertionError, match="extra_keys="):
        fw_utils.assert_like(expected, got, allow_extra=False)


def test_assert_like_dict_errors():
    with pytest.raises(AssertionError, match="!= dict"):
        fw_utils.assert_like({}, None)


def test_assert_like_list_errors():
    with pytest.raises(AssertionError, match="!= list"):
        fw_utils.assert_like([], None)
    with pytest.raises(AssertionError, match="extra_items="):
        fw_utils.assert_like([], [1])
    with pytest.raises(AssertionError, match="unexpected end of list"):
        fw_utils.assert_like([1], [])
    with pytest.raises(AssertionError, match="unexpected end of list"):
        fw_utils.assert_like([..., 1], [2])


def test_assert_like_with_callable():
    dict_callable = Mock()
    list_callable = Mock(side_effect=AssertionError("list callable"))
    expected = dict(callable=dict_callable, list=[list_callable])
    got = dict(callable="val", list=[1])
    with pytest.raises(AssertionError, match="list callable"):
        fw_utils.assert_like(expected, got)
    assert dict_callable.mock_calls == [call("val")]
    assert list_callable.mock_calls == [call(1)]
