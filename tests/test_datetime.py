"""Datetime tests."""

from datetime import datetime, timezone

import pytest

import fw_utils
from fw_utils import ZoneInfo


def test_format_datetime_truncates_microseconds():
    dt_obj = datetime(1999, 12, 31, 23, 59, 59, 999999)
    assert fw_utils.format_datetime(dt_obj) == "1999-12-31T23:59:59.999"
    dt_obj = dt_obj.replace(tzinfo=ZoneInfo("Europe/Budapest"))
    assert fw_utils.format_datetime(dt_obj) == "1999-12-31T23:59:59.999+01:00"


@pytest.mark.parametrize(
    "value,expected",
    [
        ("1999", datetime(1999, 1, 1)),
        ("19991231", datetime(1999, 12, 31)),
        ("1999-12-31", datetime(1999, 12, 31)),
        ("19991231235959", datetime(1999, 12, 31, 23, 59, 59)),
        ("1999-12-31 23:59:59", datetime(1999, 12, 31, 23, 59, 59)),
        ("1999-12-31T23_59_59.999Z", datetime(1999, 12, 31, 23, 59, 59, 999000)),
        ("1999-12-31T23-59-59.999+0100", datetime(1999, 12, 31, 22, 59, 59, 999000)),
    ],
)
def test_get_datetime(value, expected):
    expected = expected.replace(tzinfo=timezone.utc)
    assert fw_utils.get_datetime(value) == expected


def test_get_datetime_():
    # returns now in utc by default
    dt_obj = fw_utils.get_datetime()
    assert dt_obj.tzname() == "UTC"
    # returns now in the specified timezone
    dt_obj = fw_utils.get_datetime(tz="Europe/Budapest")
    assert dt_obj.tzname() in ["CET", "CEST"]


def test_get_datetime_with_sub():
    sub = (r"(\d\d\d\d)(\d\d)(\d\d).*", r"\1-\2-\3")
    dt_obj = fw_utils.get_datetime("19991231 foo", sub=sub)
    assert dt_obj == datetime(1999, 12, 31, tzinfo=timezone.utc)


def test_get_datetime_with_fmt():
    dt_obj = fw_utils.get_datetime("19991231", fmt="%Y%m%d")
    assert dt_obj == datetime(1999, 12, 31, tzinfo=timezone.utc)
    dt_obj = fw_utils.get_datetime("19991231", fmt=["%Y-%m-%d", "%Y%m%d"])
    assert dt_obj == datetime(1999, 12, 31, tzinfo=timezone.utc)


def test_get_datetime_with_tz():
    tz = ZoneInfo("Europe/Budapest")
    dt_obj = fw_utils.get_datetime("1999-12-31T23:59:59.999+0500", tz=tz)
    assert dt_obj == datetime(1999, 12, 31, 19, 59, 59, 999000, tzinfo=tz)
    dt_obj = fw_utils.get_datetime("1999-12-31T23:59:59.999", tz=tz)
    assert dt_obj == datetime(1999, 12, 31, 23, 59, 59, 999000, tzinfo=tz)


def test_get_datetime_raises_on_invalid_input():
    with pytest.raises(TypeError, match="Expected int, str or datetime"):
        fw_utils.get_datetime(object())

    with pytest.raises(ValueError, match="Can't parse 'foo' with dateutil"):
        fw_utils.get_datetime("foo")

    expected_err = r"Can't parse 'foo' with strptime: \['%Y%m%d'\]"
    with pytest.raises(ValueError, match=expected_err):
        fw_utils.get_datetime("foo", fmt=["%Y%m%d"])
