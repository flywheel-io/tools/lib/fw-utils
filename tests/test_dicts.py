"""Dict tests."""

from datetime import datetime

import pytest

import fw_utils


def test_attrdict():
    data = fw_utils.AttrDict.from_flat({"a.b": "c"})
    assert data == {"a": {"b": "c"}}
    assert data.a == data["a"] == {"b": "c"}
    assert data.a.b == data["a"]["b"] == "c"
    assert data.to_flat() == {"a.b": "c"}
    data.a.b = "d"
    assert data.a.b == "d"


def test_attrdict_raises_attrerror_on_missing_key():
    with pytest.raises(AttributeError):
        _ = fw_utils.attrify({}).key1


def test_attrify():
    data = fw_utils.attrify({"key1": [{"key2": "val2"}]})
    assert data.key1[0].key2 == "val2"


def test_flatten_dotdict():
    assert fw_utils.flatten_dotdict({"a": {"b": "c"}}) == {"a.b": "c"}


def test_inflate_dotdict():
    assert fw_utils.inflate_dotdict({"a.b": "c"}) == {"a": {"b": "c"}}


def test_get_field_does_not_raise_on_missing_field():
    assert fw_utils.get_field(object(), "x") is None
    assert fw_utils.get_field(object(), "x.y") is None


def test_get_field_raises_on_unrelated_attribute_error():
    class A:
        y = property(lambda self: self.do())

    class B:
        x = A()

    with pytest.raises(AttributeError, match="object has no attribute 'do'"):
        fw_utils.get_field(A(), "y")

    with pytest.raises(AttributeError, match="object has no attribute 'do'"):
        fw_utils.get_field(B(), "x.y")


def test_clean_metadata_dict_nan():
    "Test that all NaNs (even inside iterables) changed to None"
    nan = float("NaN")
    d = {"a": nan, "b": [1, nan, 3], "c": {"d": 1, "e": nan}}
    assert d["a"] is not None  # just to prove Nan != None to python

    clean_d = fw_utils.dicts.clean_metadata_dict(d)

    assert clean_d["a"] is None
    assert clean_d["b"][1] is None
    assert clean_d["c"]["e"] is None


def test_clean_metadata_dict_datetime():
    "Test that all datetimes (even inside iterables) changed to str"
    now = datetime.now()
    d = {"a": now, "b": ["str", now, 3], "c": {"d": 1, "e": now}}
    now_str = now.isoformat(timespec="milliseconds")
    assert d["a"] != now_str  # just to prove datetime

    clean_d = fw_utils.dicts.clean_metadata_dict(d)

    assert clean_d["a"] == now_str
    assert now_str in clean_d["b"]  # sets are unordered
    assert clean_d["c"]["e"] == now_str
